document.getElementById("add-to-cart").addEventListener("click", function () {
  var url = document.getElementById("url").value;
  var name = document.getElementById("name").value;
  var price = document.getElementById("price").value;
  var currency = document.getElementById("currency").value;

  var cart = JSON.parse(localStorage.getItem("cart")) || [];
  cart.push({ url, name, price, currency, favorite: false });
  localStorage.setItem("cart", JSON.stringify(cart));

  updateCart();
});

function updateCart() {
  var cart = JSON.parse(localStorage.getItem("cart")) || [];
  var tbody = document.getElementById("cart").getElementsByTagName("tbody")[0];
  tbody.innerHTML = "";

  var total = 0;
  for (var i = 0; i < cart.length; i++) {
    var row = tbody.insertRow();
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);
    var cell4 = row.insertCell(3);

    cell1.innerHTML = `<i id="star-${i}" class="far fa-star cursor-pointer"></i> <a href="${cart[i].url}" target="_blank">${cart[i].name}</a>`;
    document.getElementById(`star-${i}`).addEventListener("click", function () {
      this.classList.toggle("fas");
      this.classList.toggle("far");
    });
    cell2.innerHTML = cart[i].price;
    cell3.innerHTML = cart[i].currency;
    cell4.innerHTML =
      '<button onclick="removeFromCart(' +
      i +
      ')" class="bg-red-500 hover:bg-red-700 text-white font-bold py-1 px-2 rounded">Remove</button>';

    total += parseFloat(cart[i].price);
  }

  document.getElementById("total").innerText = total;
}

function removeFromCart(index) {
  var cart = JSON.parse(localStorage.getItem("cart")) || [];
  cart.splice(index, 1);
  localStorage.setItem("cart", JSON.stringify(cart));

  updateCart();
}

updateCart();
